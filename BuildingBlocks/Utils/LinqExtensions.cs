using System.Linq;
using System.Collections.Generic;

namespace BuildingBlocks.Utils
{
    public static class LinqExtensions
    {
        // https://stackoverflow.com/a/13731854
        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> items, int maxItems)
        {
            return items.Select((item, inx) => new { item, inx })
                        .GroupBy(x => x.inx / maxItems)
                        .Select(g => g.Select(x => x.item));
        }
    }
}