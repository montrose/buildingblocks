using System;

namespace BuildingBlocks.Utils
{
    public static class TimeUtils
    {
        public static DateTime NormalizeTicks(this DateTime date)
        {
            return date.AddTicks(-date.Ticks % TimeSpan.TicksPerMillisecond);
        }
    }
}