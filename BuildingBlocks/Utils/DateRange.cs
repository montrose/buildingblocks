using System;

namespace BuildingBlocks.Utils
{
    public class DateRange
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public TimeSpan Range => End - Start;

        public DateRange(DateTime start)
        {
            Start = start;
            End = Start.AddHours(1);
        }

        public DateRange(DateTime start, DateTime end)
        {
            if (start >= end)
                throw new ArgumentException("Start time must not be greater than end time");

            Start = start;
            End = end;
        }

        public bool Contains(DateTime date)
        {
            return Start <= date && End >= date;
        }

        public override string ToString()
        {
            return $"{Start}-{End}";
        }
    }
}