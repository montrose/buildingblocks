using System;
using System.Text;
using System.Security.Cryptography;

namespace BuildingBlocks.Utils
{
    public static class StringUtils
    {
        public static string ToMD5Hex(string text)
        {
            byte[] hash;
            using (var md5 = MD5.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(text);
                hash = md5.ComputeHash(bytes);
            }
            return BitConverter.ToString(hash).Replace("-", "");
        }
    }
}