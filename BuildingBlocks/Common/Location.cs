using System;
using MongoDB.Driver.GeoJsonObjectModel;

namespace BuildingBlocks.Common
{
    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Location(double latitude, double longitude)
        {
            ValidateCoords(latitude, longitude);

            Latitude = latitude;
            Longitude = longitude;
        }

        public GeoJsonPoint<GeoJson2DGeographicCoordinates> ToGeoBson()
        {
            // return GeoJson.Point(GeoJson.Geographic(Longitude, Latitude));
            return GeoJson.Point(new GeoJson2DGeographicCoordinates(Longitude, Latitude));
        }

        public static void ValidateCoords(double latitude, double longitude)
        {
            if (latitude > 90 || latitude < -90)
                throw new ArgumentException("Latitude value must be between -90 and 90");

            if (longitude > 180 || longitude < -180)
                throw new ArgumentException("Longitude values must be between -180 and 180");
        }
    }
}