using System;
using MongoDB.Driver;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BuildingBlocks.Database
{
    using Common;

    public interface IDocumentCollection<TEnt> where TEnt : MongoEntity
    {
        IMongoIndexManager<TEnt> Indexes { get; }
        void Clear();
        Task<long> Count();
        Task<long> CountWhere(Expression<Func<TEnt, bool>> filter);
        Task InsertAsync(TEnt entity);
        Task InsertManyAsync(IEnumerable<TEnt> entities);
        Task UpdateAsync(TEnt update, bool upsert = true);
        Task ReplaceManyAsync(IEnumerable<TEnt> updates);
        Task<TEnt> FindOneOrDefault(Expression<Func<TEnt, bool>> filter);
        Task<IEnumerable<TEnt>> FindMany(Expression<Func<TEnt, bool>> filter);
        Task<IEnumerable<TEnt>> FindManyPaged(Expression<Func<TEnt, bool>> filter, DbPage page);
        Task<IEnumerable<TEnt>> FindAllNear(Expression<Func<TEnt, object>> field, Location location, double maxKmDistance);
        Task<IEnumerable<TEnt>> FindAllNear(string field, Location location, double maxKmDistance);
        Task<IEnumerable<TEnt>> FindNearWhere(
            Expression<Func<TEnt, object>> field, 
            Expression<Func<TEnt, bool>> condition, 
            Location location, 
            double maxKmDistance);
        Task<IEnumerable<TEnt>> FindNearWhere(
            string field, 
            Expression<Func<TEnt, bool>> condition, 
            Location location, 
            double maxKmDistance);
        Task<bool> RemoveOneAsync(Expression<Func<TEnt, bool>> filter);
        Task<long> RemoveManyAsync(Expression<Func<TEnt, bool>> filter);
    }
}