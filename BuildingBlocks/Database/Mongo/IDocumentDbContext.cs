using MongoDB.Driver;
using System.Collections.Generic;

namespace BuildingBlocks.Database
{
    public interface IDocumentDbContext
    {
        IMongoClient Client { get; }
        IMongoDatabase Database { get; }
        void CreateCollections();
        void RemoveCollections();
        void CreateIndexes();
        void EnsureCollectionsCreated();
        List<string> CollectionNames { get; }
    }
}