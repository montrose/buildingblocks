using System;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace BuildingBlocks.Database
{
    using Common;

    public class DocumentCollection<TEnt> : IDocumentCollection<TEnt> where TEnt : MongoEntity
    {
        private readonly IMongoCollection<TEnt> _collection;
        public IMongoIndexManager<TEnt> Indexes { get; private set; }

        public DocumentCollection(IMongoCollection<TEnt> collection)
        {
            _collection = collection;
            Indexes = _collection.Indexes;
        }

        public void Clear()
        {
            _collection.DeleteMany(x => true);
        }

        public async Task<long> Count()
        {
            return await _collection.CountDocumentsAsync(x => true);
        }

        public async Task<long> CountWhere(Expression<Func<TEnt, bool>> filter)
        {
            return await _collection.CountDocumentsAsync(filter);
        }

        public async Task InsertAsync(TEnt entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task InsertManyAsync(IEnumerable<TEnt> entities)
        {
            await _collection.InsertManyAsync(entities, new InsertManyOptions{IsOrdered = false});
        }

        public async Task UpdateAsync(TEnt update, bool upsert = true)
        {
            update.UpdatedAt = DateTime.UtcNow;

            var entity = FindOneOrDefault(x => x.Id == update.Id);
            if (entity == default && upsert)
            {
                await InsertAsync(update);
                return;
            }

            await _collection.ReplaceOneAsync(x => x.Id == update.Id, update);
        }

        public async Task ReplaceManyAsync(IEnumerable<TEnt> updates)
        {
            int count = updates.Count();
            var models = new WriteModel<TEnt>[count];

            for (var i = 0; i < count; i++)
            {
                var doc = updates.ElementAt(i);
                models[i] = new ReplaceOneModel<TEnt>(new BsonDocument("_id", doc.Id), doc) {IsUpsert = true};
            }

            await _collection.BulkWriteAsync(models);
        }

        public async Task<TEnt> FindOneOrDefault(Expression<Func<TEnt, bool>> filter)
        {
            return await _collection.Find(filter)
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<TEnt>> FindMany(Expression<Func<TEnt, bool>> filter)
        {
            return await _collection.Find(filter)
                .ToListAsync();
        }

        public async Task<IEnumerable<TEnt>> FindAllNear(Expression<Func<TEnt, object>> field, Location location, double maxKmDistance)
        {
            var filter = Builders<TEnt>.Filter
                .NearSphere(field, location.ToGeoBson(), maxKmDistance * 1000);  // convert from meters

            return await _collection.Find(filter).ToListAsync();
        }

        public async Task<IEnumerable<TEnt>> FindAllNear(string field, Location location, double maxKmDistance)
        {
            var filter = Builders<TEnt>.Filter
                .NearSphere(field, location.ToGeoBson(), maxKmDistance * 1000);

            return await _collection.Find(filter).ToListAsync();
        }

        public async Task<IEnumerable<TEnt>> FindNearWhere(
            Expression<Func<TEnt, object>> field, 
            Expression<Func<TEnt, bool>> condition, 
            Location location, 
            double maxKmDistance)
        {
            var builder = Builders<TEnt>.Filter;

            var conditionFilter = builder.Where(condition);
            var nearFilter = builder
                .NearSphere(field, location.ToGeoBson(), maxKmDistance * 1000);
            var filter = builder.And(conditionFilter, nearFilter);

            return await _collection.Find(filter).ToListAsync();    
        }

        public async Task<IEnumerable<TEnt>> FindNearWhere(
            string field, 
            Expression<Func<TEnt, bool>> condition, 
            Location location, 
            double maxKmDistance)
        {
            var builder = Builders<TEnt>.Filter;

            var conditionFilter = builder.Where(condition);
            var nearFilter = builder
                .NearSphere(field, location.ToGeoBson(), maxKmDistance * 1000);
            var filter = builder.And(conditionFilter, nearFilter);

            return await _collection.Find(filter).ToListAsync();    
        }

        public async Task<bool> RemoveOneAsync(Expression<Func<TEnt, bool>> filter)
        {
            var result = await _collection.DeleteOneAsync(filter);
            return result.DeletedCount > 0;
        }

        public async Task<long> RemoveManyAsync(Expression<Func<TEnt, bool>> filter)
        {
            var result = await _collection.DeleteManyAsync(filter);
            return result.DeletedCount;
        }

        public async Task<IEnumerable<TEnt>> FindManyPaged(Expression<Func<TEnt, bool>> filter, DbPage page)
        {
            return await _collection.Find(filter)
                .SortByDescending(x => x.Id)
                .Skip(page.Skip)
                .Limit(page.PageSize)
                .ToListAsync();
        }
    }
}