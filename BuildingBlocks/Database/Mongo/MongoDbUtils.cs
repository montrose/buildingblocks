using System;
using MongoDB.Driver;
using System.Linq.Expressions;

namespace BuildingBlocks.Database
{
    public static class MongoDbUtils
    {
        public static CreateIndexModel<T> BuildSingleIndex<T>(
            Expression<Func<T, object>> field,
            bool ascending = true, 
            CreateIndexOptions options = null) where T : MongoEntity
        {
            var indexField = new ExpressionFieldDefinition<T>(field);
            IndexKeysDefinition<T> indexKey;

            if (ascending)
                indexKey = new IndexKeysDefinitionBuilder<T>().Ascending(indexField);
            else
                indexKey = new IndexKeysDefinitionBuilder<T>().Descending(indexField);

            return new CreateIndexModel<T>(indexKey, options);
        }

        public static CreateIndexModel<T> BuildSingleIndex<T>(
            string field,
            bool ascending = true, 
            CreateIndexOptions options = null) where T : MongoEntity
        {
            var indexField = new StringFieldDefinition<T>(field);
            IndexKeysDefinition<T> indexKey;

            if (ascending)
                indexKey = new IndexKeysDefinitionBuilder<T>().Ascending(indexField);
            else
                indexKey = new IndexKeysDefinitionBuilder<T>().Descending(indexField);

            return new CreateIndexModel<T>(indexKey, options);
        }

        public static CreateIndexModel<T> BuildGeo2DSphereIndex<T>(
            Expression<Func<T, object>> field,
            CreateIndexOptions options = null)
        {
            var builders = Builders<T>.IndexKeys;

            return new CreateIndexModel<T>(
                builders.Geo2DSphere(field).Ascending(field),
                options
            );
        }

        public static CreateIndexModel<T> BuildGeo2DSphereIndex<T>(
            string field,
            CreateIndexOptions options = null)
        {
            var builders = Builders<T>.IndexKeys;

            return new CreateIndexModel<T>(
                builders.Geo2DSphere(field),
                options
            );
        }
    }
}