using System;
using MongoDB.Bson;

namespace BuildingBlocks.Database
{
    public class MongoEntity
    {
        public ObjectId Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public MongoEntity()
        {
            Id = ObjectId.GenerateNewId();
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}

