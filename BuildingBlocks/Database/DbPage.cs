using System;

namespace BuildingBlocks.Database
{
    public class DbPage
    {
        public int PageSize { get; }
        public int PageNumber { get; }
        public int Skip => PageSize * (PageNumber - 1);  // 1-based to reflect client API queries

        public DbPage()
        {
            PageNumber = 1;
            PageSize = 100;  // arbitrary
        }

        public DbPage(int pageNumber, int pageSize)
        {
            if (pageNumber <= 0 || pageSize <= 0)
                throw new ArgumentException("Page number and page size must be greater than 0");

            PageNumber = pageNumber;
            PageSize = pageSize;
        }
    }
}